Page({
  data:{
    imgUrls: [
      '/assets/img/banner1.jpg',
      '/assets/img/banner.jpg',
    ],
    aboutUs:[],
    recommonds:[
     
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 3000,
    duration: 1000,
    dotsColor:'#848282',
    dotsChoice:'#9b0c17',
    movies: [],
    hidden: false
  },
  onload:function(){

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})