Page({
  data: {
    goodsList:{
      saveHidden: true,
      totalPrice: 0,
      totalScoreToPay: 0,
      allSelect: true,
      noSelect: false,
      num: 0,
      list: [{ "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "price": 59, "number": 1, "userId": 951, "ordernum": 9090980, "orderStatus": 0, "left": "", "active": 'true' }, { "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "price": 59, "number": 1, "userId": 951, "ordernum": 9090980, "orderStatus": 0, "left": "", "active": 'true' },{ "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "price": 59, "number": 1, "userId": 951, "ordernum": 9090980, "orderStatus": 0, "left": "", "active": 'true' }, { "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "price": 59, "number": 1, "userId": 951, "ordernum": 9090980, "orderStatus": 0, "left": "", "active": 'true' },{ "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "price": 59, "number": 1, "userId": 951, "ordernum": 9090980, "orderStatus": 0, "left": "", "active": 'true' }, { "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "price": 59, "number": 1, "userId": 951, "ordernum": 9090980, "orderStatus": 0, "left": "", "active": 'true' }]
    },
    delBtnWidth: 120, 
  },
  onLoad: function () {
    this.initEleWidth();
    this.onShow();
  },
  onShow(){
    
    var shopList=this.data.goodsList.list;
    this.setGoodsList(this.getSaveHide(), this.totalPrice(), this.allSelect(), this.noSelect(), this.num(), shopList);
  },
  //获取元素自适应后的实际宽度
  getEleWidth: function (w) {
    var real = 0;
    try {
      var res = wx.getSystemInfoSync().windowWidth;
      var scale = (750 / 2) / (w / 2);
      real = Math.floor(res / scale);
      return real;
    } catch (e) {
      return false;
    }
  },
  initEleWidth: function () {
    var delBtnWidth = this.getEleWidth(this.data.delBtnWidth);
    this.setData({
      delBtnWidth: delBtnWidth
    });
  },
  touchS: function (e) {
    if (e.touches.length == 1) {
      this.setData({
        startX: e.touches[0].clientX
      });
    }
  },
  touchM: function (e) {
    var index = e.currentTarget.dataset.index;

    if (e.touches.length == 1) {
      var moveX = e.touches[0].clientX;
      var disX = this.data.startX - moveX;
      var delBtnWidth = this.data.delBtnWidth;
      var left = "";
      if (disX == 0 || disX < 0) {//如果移动距离小于等于0，container位置不变
        left = "transform:translateX(0)";
      } else if (disX > 0) {//移动距离大于0，container left值等于手指移动距离
       
        left = 'transform: translateX(-'+disX+'px'+')';
        if (disX >= delBtnWidth) {
          left = 'transform: translateX(-' + delBtnWidth + 'px' +')';   
        }
      }
      var list = this.data.goodsList.list;
      if (index != "" && index != null) {
        list[parseInt(index)].left = left;
        this.setGoodsList(this.getSaveHide(), this.totalPrice(), this.allSelect(), this.noSelect(), this.num(),list);
      }
    }
  },

  touchE: function (e) {
    var index = e.currentTarget.dataset.index;
    if (e.changedTouches.length == 1) {
      var endX = e.changedTouches[0].clientX;
      var disX = this.data.startX - endX;
      var delBtnWidth = this.data.delBtnWidth;
      var left = disX > delBtnWidth / 2 ? 'transform:translateX(-' + delBtnWidth + 'px' + ')' :'transform:translateX(0)';
     
      var list = this.data.goodsList.list;
      if (index !== "" && index != null) {
        list[parseInt(index)].left = left;
        this.setGoodsList(this.getSaveHide(), this.totalPrice(), this.allSelect(), this.noSelect(), this.num(),list);

      }
    }
  },
  getSaveHide: function () {
    var saveHidden = this.data.goodsList.saveHidden;
    return saveHidden;
  },
  //  删除
   delItem: function (e) {
     var index = e.currentTarget.dataset.index;
     console.log(index);
     var list = this.data.goodsList.list;
     list.splice(index, 1);
     this.setGoodsList(this.getSaveHide(), this.totalPrice(), this.allSelect(), this.noSelect(), this.num(),list);
   },
  //  选中
  selectTap: function (e) {
    var index = e.currentTarget.dataset.index;
    var list = this.data.goodsList.list;
    if (index !== "" && index != null) {
      list[parseInt(index)].active = !list[parseInt(index)].active;
      this.setGoodsList(this.getSaveHide(), this.totalPrice(), this.allSelect(), this.noSelect(), this.num(),list);
    }
  },
  // 设置data中goodsList
  setGoodsList: function (saveHidden, total, allSelect, noSelect, num,list) {
    this.setData({
      goodsList: {
        saveHidden: saveHidden,
        totalPrice: total,
        allSelect: allSelect,
        noSelect: noSelect,
        list: list,
        num:num
      }
    });
    // var shopCarInfo = {};
    // var tempNumber = 0;
    // shopCarInfo.shopList = list;
    // for (var i = 0; i < list.length; i++) {
    //   tempNumber = tempNumber + list[i].number
    // }
    // shopCarInfo.shopNum = tempNumber;
    // wx.setStorage({
    //   key: "shopCarInfo",
    //   data: shopCarInfo
    // })
  },
  // 加
  addBtnTap: function (e) {
    var index = e.currentTarget.dataset.index;
    var list = this.data.goodsList.list;
    if (index !== "" && index != null) {
      if (list[parseInt(index)].number < 100) {
        list[parseInt(index)].number++;
        this.setGoodsList(this.getSaveHide(), this.totalPrice(), this.allSelect(), this.noSelect(),this.num(), list);
      }
    }
  },
  // 减
  ruduceBtnTap: function (e) {
    var index = e.currentTarget.dataset.index;
    var list = this.data.goodsList.list;
    if (index !== "" && index != null) {
      if (list[parseInt(index)].number > 1) {
        list[parseInt(index)].number--;
        this.setGoodsList(this.getSaveHide(), this.totalPrice(), this.allSelect(), this.noSelect(), this.num(),list);
      }
    }
  },
  allSelect: function () {
    var list = this.data.goodsList.list;
    var allSelect = false;
    for (var i = 0; i < list.length; i++) {
      var curItem = list[i];
      if (curItem.active) {
        allSelect = true;
      } else {
        allSelect = false;
        break;
      }
    }
    return allSelect;
  },
  // 未选中
  noSelect: function () {
    var list = this.data.goodsList.list;
    var noSelect = 0;
    for (var i = 0; i < list.length; i++) {
      var curItem = list[i];
      if (!curItem.active) {
        noSelect++;
      }
    }
    if (noSelect == list.length) {
      return true;
    } else {
      return false;
    }
  },
  num:function(){
    var list = this.data.goodsList.list;
    var num = 0;
    for (var i = 0; i < list.length; i++) {
      var curItem = list[i];
      if (curItem.active) {
        num += curItem.number;
      }
    }
    return num;
  },
  totalPrice: function () {
    var list = this.data.goodsList.list;
    var total = 0;
    var num=0;
    for (var i = 0; i < list.length; i++) {
      var curItem = list[i];
      if (curItem.active) {
        num += curItem.number;
        total += parseFloat(curItem.price) * curItem.number;
      }
    }
    this.data.goodsList.num = num;
    total = parseFloat(total.toFixed(2));//js浮点计算bug，取两位小数精度
    return total;
  },
  bindAllSelect: function () {
    var currentAllSelect = this.data.goodsList.allSelect;
    var list = this.data.goodsList.list;
    if (currentAllSelect) {
      for (var i = 0; i < list.length; i++) {
        var curItem = list[i];
        curItem.active = false;
      }
    } else {
      for (var i = 0; i < list.length; i++) {
        var curItem = list[i];
        curItem.active = true;
      }
    }
    this.setGoodsList(this.getSaveHide(), this.totalPrice(), !currentAllSelect, this.noSelect(), this.num(),list);
  }
})