// pages/orderList/orderList.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusType: ["全部", "待付款", "待评价"],
    currentType: 0,
    tabClass: ["", "", ""],
    orderList: [{ "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "total": 59, "num": 1, "userId": 951, "ordernum": 9090980, "orderStatus": 0 }, { "avator": "../../assets/img/food.png", "name": "澳洲大鸡排", "icon": "", "total": 60, "num": 100, "userId": 951, "ordernum": 9090980, "orderStatus": 1 }, { "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "total": 59, "num": 1000, "userId": 951, "ordernum": 9090980, "orderStatus": 2 }, { "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "total": 59, "num": 1, "userId": 951, "ordernum": 9090980, "orderStatus": 0 }, { "avator": "../../assets/img/food.png", "name": "澳洲厚切", "icon": "", "total": 59, "num": 1, "userId": 951, "ordernum": 9090980, "orderStatus": 0 }]
  },
  statusTap:function(e){
    var curType = e.currentTarget.dataset.index;
    this.data.currentType = curType
    this.setData({
      currentType: curType
    });
  },
  deleteOrder:function(e){
    var orderId = e.currentTarget.dataset.id;
    wx.showModal({
      title: '确定要取消该订单吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {
          console.log(orderId);
        }
      }
    })
  },
  /**点击跳转到订单详情 */
  toDetail:function(){
    wx.navigateTo({
      url: '../orderDetail/orderDetail'
    })
  },
  /**跳转到评价页*/
  toEvaluate: function () {
    wx.navigateTo({
      url: '../evaluate/evaluate'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // wx.showLoading();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})